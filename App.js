import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Ionicons, AntDesign } from '@expo/vector-icons';
import { Provider as PaperProvider } from 'react-native-paper';
import { ActivityIndicator, TouchableRipple, Colors } from 'react-native-paper';
import firebase from './database/firebaseDb'

// Auth Screen
import LoginScreen from './screens/auth/LoginScreen';
import SignUpScreen from './screens/auth/SignUpScreen';

// Statistic Screen
import Client from './screens/module/Statistic/Client'

// Question Screen
import AddQuestionScreen from './screens/module/Questions/AddQuestionScreen';
import QuestionScreen from './screens/module/Questions/QuestionScreen';
import QuestionDetailScreen from './screens/module/Questions/QuestionDetailScreen';

// Survey Screen
import SurveyScreen from './screens/module/Survey/SurveyScreen';
import SurveyedScreen from './screens/module/Survey/SurveyedScreen'

// Relawan Screen
import RelawanScreen from './screens/module/Admin/RelawanScreen'
import RelawanDetailScreen from './screens/module/Admin/RelawanDetailScreen'

// Component
import Statistic from './screens/components/Statistic'
import { DrawerContent } from './screens/components/Drawer';

import {decode, encode} from 'base-64'

if (!global.btoa) { global.btoa = encode }

if (!global.atob) { global.atob = decode }

const Stack = createStackNavigator();

function Relawan({ navigation }) {
  return (
    <Stack.Navigator screenOptions={{
      headerStyle: {
      backgroundColor: Colors.blue400,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
      fontWeight: 'bold'
      },
      headerLeftContainerStyle: {
        marginLeft: 15
      }
  }}>
      <Stack.Screen 
        name="RelawanScreen" 
        component={RelawanScreen} 
        options={{ title: 'Relawan', headerLeft: () => (
          <TouchableRipple style={{ width: 30, height: 30 }}>
            <Ionicons name="ios-menu" size={25} color="#fff" onPress={() => navigation.openDrawer()} />
          </TouchableRipple>
        ), }}
      />
      <Stack.Screen 
        name="RelawanDetailScreen" 
        component={RelawanDetailScreen} 
        options={{ title: 'Edit Relawan' }}
      />
    </Stack.Navigator>
  );
}


function HomeScreen({ navigation }) {
  return (
    <Stack.Navigator screenOptions={{
      headerStyle: {
      backgroundColor: Colors.blue400,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
      fontWeight: 'bold'
      },
      headerLeftContainerStyle: {
        marginLeft: 15
      }
  }}>
      <Stack.Screen 
      name="SurveyedScreen" 
      component={SurveyedScreen} 
      options={{
        headerLeft: () => (
          <TouchableRipple style={{ width: 30, height: 30 }}>
            <Ionicons name="ios-menu" size={25} color="#fff" onPress={() => navigation.openDrawer()} />
          </TouchableRipple>
        ),
        title: "Lembar Kontrol"
      }}
      />
      <Stack.Screen 
      name="SurveyScreen"
      component={SurveyScreen} 
      options={{ title: 'Lembar Survei', headerLeft: null }}
      />
    </Stack.Navigator>
  );
}

function ClientScreen({ navigation }) {
  return (
    <Stack.Navigator screenOptions={{
      headerStyle: {
      backgroundColor: Colors.blue400,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
      fontWeight: 'bold'
      },
      headerLeftContainerStyle: {
        marginLeft: 15
      }
  }}>
      <Stack.Screen 
        name="Client" 
        component={Client} 
        options={{ title: 'Home', headerLeft: () => (
          <TouchableRipple style={{ width: 30, height: 30 }}>
            <Ionicons name="ios-menu" size={25} color="#fff" onPress={() => navigation.openDrawer()} />
          </TouchableRipple>
        ), }}
      />
    </Stack.Navigator>
  );
}

function SettingsScreen({ navigation }) {
      return (
        <Stack.Navigator screenOptions={{
          headerStyle: {
          backgroundColor: Colors.blue400,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold'
          },
          headerLeftContainerStyle: {
            marginLeft: 15
          }
      }}>
          <Stack.Screen 
            name="AddQuestionScreen" 
            component={AddQuestionScreen} 
            options={{ title: 'Tambah Pertanyaan', headerLeft: () => (
              <TouchableRipple style={{ width: 30, height: 30 }}>
                <Ionicons name="ios-menu" size={25} color="#fff" onPress={() => navigation.openDrawer()} />
              </TouchableRipple>
            ), }}
          />
          <Stack.Screen 
            name="QuestionScreen"
            component={QuestionScreen} 
            options={{ title: 'Pertanyaan' }}
          />
          <Stack.Screen 
            name="QuestionDetailScreen" 
            component={QuestionDetailScreen} 
            options={{ title: 'Edit Pertanyaan' }}
          />
        </Stack.Navigator>
      );
    }

function StatisticScreen({ navigation }) {
  return (
    <Stack.Navigator screenOptions={{
      headerStyle: {
      backgroundColor: Colors.blue400,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
      fontWeight: 'bold'
      },
      headerLeftContainerStyle: {
        marginLeft: 15
      }
  }}>
      <Stack.Screen
        name="Statistic"
        component={Statistic}
        options={{
          headerTitle: "Statistik",
          headerLeft: () => (
            <TouchableRipple style={{ width: 30, height: 30 }}>
              <Ionicons name="ios-menu" size={25} color="#fff" onPress={() => navigation.openDrawer()} />
            </TouchableRipple>
          )
        }}
      />
    </Stack.Navigator>
  )
}

function AuthStack() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
      />
      <Stack.Screen
        name="SignUpScreen"
        component={SignUpScreen}
      />
    </Stack.Navigator>
  );
}

const Drawer = createDrawerNavigator();

function DrawerNav() {
  return (
    <Drawer.Navigator initialRouteName="Home" drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen name="Home" component={HomeTabs} />
    </Drawer.Navigator>
  );
}

class HomeTabs extends React.Component {
  constructor() {
    super();
    this.state = {
      role: '',
      isLoading: true
    }
  }

  async componentDidMount() {
    var user = firebase.auth().currentUser
      if (user) {
        this.setState({
          isLoading: true
        })
        let id = user.uid   
        if (id) {
            let ref = firebase.firestore().collection('users').doc(id)
            ref.get().then((res) => {
              let data = res.data()
              let { role } = data
              this.setState({
                isLoading: false,
                role: role
              })
            })
        }
      }
  }

  render () {
    let { role, isLoading } = this.state

    if(isLoading) {
      return (
        <ActivityIndicator />
      )
    }

    if (role === 'Super-Admin') {
      return (
        <PaperProvider>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
              let iconType;
              if (route.name === 'Beranda') {
                iconType = 'Ionicons'
                iconName = 'ios-home';
              } else if (route.name === 'Pertanyaan') {
                iconType = 'Ionicons'
                iconName = 'ios-list';
              } else if (route.name === 'Relawan') {
                iconType = 'Ionicons'
                iconName = 'ios-people';
              } else if (route.name === 'Statistic') {
                iconType = 'Ionicons'
                iconName = 'ios-stats';
              }
              // You can return any component that you like here!
              if (iconType === 'Ionicons') {
                return <Ionicons name={iconName} size={size} color={color} />;
              } else if (iconType === 'AntDesign') {
                return <AntDesign name={iconName} size={size} color={color} />;
              }
            },
          })}
          tabBarOptions={{
            activeTintColor: Colors.blue400,
            inactiveTintColor: Colors.grey500,
          }}
        >
          <Tab.Screen name="Beranda" component={HomeScreen} />
          <Tab.Screen name="Pertanyaan" component={SettingsScreen} />
          <Tab.Screen name="Relawan" component={Relawan} />
          <Tab.Screen name="Statistic" component={StatisticScreen} />
        </Tab.Navigator>
        </PaperProvider>
      );
    } else if (role === 'Admin') {
      return (
        <PaperProvider>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
              let iconType;
              if (route.name === 'Beranda') {
                iconType = 'Ionicons'
                iconName = 'ios-home';
              }
              // You can return any component that you like here!
              if (iconType === 'Ionicons') {
                return <Ionicons name={iconName} size={size} color={color} />;
              } else if (iconType === 'AntDesign') {
                return <AntDesign name={iconName} size={size} color={color} />;
              }
            },
          })}
          tabBarOptions={{
            activeTintColor: Colors.blue400,
            inactiveTintColor: Colors.grey500,
          }}
        >
          <Tab.Screen name="Beranda" component={HomeScreen} />
        </Tab.Navigator>
        </PaperProvider>
      )
    } else if (role === 'Klien') {
      return (
        <PaperProvider>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
              let iconType;
              if (route.name === 'Beranda') {
                iconType = 'Ionicons'
                iconName = 'ios-home';
              } else if (route.name === 'Pertanyaan') {
                iconType = 'Ionicons'
                iconName = 'ios-list';
              } else if (route.name === 'Statistic') {
                iconType = 'Ionicons'
                iconName = 'ios-stats';
              }
              // You can return any component that you like here!
              if (iconType === 'Ionicons') {
                return <Ionicons name={iconName} size={size} color={color} />;
              } else if (iconType === 'AntDesign') {
                return <AntDesign name={iconName} size={size} color={color} />;
              }
            },
          })}
          tabBarOptions={{
            activeTintColor: Colors.blue400,
            inactiveTintColor: Colors.grey500,
          }}
        >
          <Tab.Screen name="Statistic" component={ClientScreen} />
        </Tab.Navigator>
        </PaperProvider>
      )
    }
  }
}

const Tab = createBottomTabNavigator();

export default class App extends React.Component{
  render () {
    return (
      <PaperProvider>
        <NavigationContainer>
          <Stack.Navigator headerMode="none">
            <Stack.Screen name="Auth" component={AuthStack} />
            <Stack.Screen name="Draw" component={DrawerNav} />
          </Stack.Navigator>
        </NavigationContainer>
      </PaperProvider>
    );
  }
}