// screens/UserScreen.js

import React, { Component } from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';
import { ActivityIndicator, Colors } from 'react-native-paper';
import { ListItem } from 'react-native-elements'
import firebase from '../../../database/firebaseDb';

class QuestionScreen extends Component {

  constructor() {
    super();
    this.firestoreRef = firebase.firestore().collection('questions');
    this.state = {
      isLoading: true,
      userArr: [],
    };
  }

  componentDidMount() {
    this.unsubscribe = this.firestoreRef.onSnapshot(this.getCollection);
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  getCollection = (querySnapshot) => {
    const userArr = [];
    querySnapshot.forEach((res) => {
      const { question } = res.data();
      userArr.push({
        key: res.id,
        res,
        question
      });
    });
    this.setState({
      userArr,
      isLoading: false,
   });
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" style={styles.preloader} animating={true} color={Colors.blue400} />
        </View>
      )
    }    
    return (
      <ScrollView style={styles.container}>
          {
            this.state.userArr.map((item, i) => {
              return (
                <ListItem
                  key={i}
                  chevron
                  bottomDivider
                  title={i, item.question}
                  onPress={() => {
                    this.props.navigation.navigate('QuestionDetailScreen', {
                      userkey: item.key
                    });
                  }}/>
              );
            })
          }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingBottom: 22
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default QuestionScreen;