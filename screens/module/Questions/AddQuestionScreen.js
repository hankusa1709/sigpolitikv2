import React, { Component } from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';
import { Card, Title, Paragraph, Divider, TextInput, Button, DataTable, Surface, Dialog, Portal, ActivityIndicator, Colors, Avatar, IconButton } from 'react-native-paper';
import firebase from '../../../database/firebaseDb';

class AddUserScreen extends Component {
  constructor() {
    super();
    this.dbRef = firebase.firestore().collection('questions');
    this.state = {
      question: '',
      a: {
        choice: '',
        label: 'a'
      },
      b: {
        choice: '',
        label: 'b'
      },
      c: {
        choice: '',
        label: 'c'
      },
      d: {
        choice: '',
        label: 'd'
      },
      isLoading: false,
      visible: false,
    };
  }

  _showDialog = () => this.setState({ visible: true });

  _hideDialog = () => this.setState({ visible: false });

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  storeQuestion() {
    if(this.state.question === '' || this.state.a === '' || this.state.b === '' || this.state.c === '' || this.state.d === ''){
     this._showDialog()
    } else {
      this.setState({
        isLoading: true,
      });      
      this.dbRef.add({
        question: this.state.question,
        a: this.state.a,
        b: this.state.b,
        c: this.state.c,
        d: this.state.d,
      }).then((res) => {
        this.setState({
          question: '',
          a: {
            choice: '',
            label: 'a'
          },
          b: {
            choice: '',
            label: 'b'
          },
          c: {
            choice: '',
            label: 'c'
          },
          d: {
            choice: '',
            label: 'd'
          },
          isLoading: false,
        });
        this.props.navigation.navigate('QuestionScreen')
      })
      .catch((err) => {
        console.error("Error found: ", err);
        this.setState({
          isLoading: false,
        });
      });
    }
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" style={styles.preloader} animating={true} color={Colors.blue400} />
        </View>
      )
    }
    return (
      <ScrollView style={styles.container}>
        <Portal>
          <Dialog
             visible={this.state.visible}
             onDismiss={this._hideDialog}>
            <Dialog.Title>Error!</Dialog.Title>
            <Dialog.Content>
              <Paragraph>Tolong Lengkapi Kolom!</Paragraph>
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={this._hideDialog}>OK</Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
        <TextInput
          mode="flat"
          style={styles.inputStyle}
          label="Pertanyaan"
          value={this.state.question}
          onChangeText={(val) => this.updateInputVal(val, 'question')}
        />
        <TextInput
          mode="flat"
          style={styles.inputStyle}
          label="A"
          value={this.state.a.choice}
          onChangeText={(text) => 
            this.setState({a: {...this.state.a,choice: text}}
          )}
        />
        <TextInput
          mode="flat"
          style={styles.inputStyle}
          label="B"
          value={this.state.b.choice}
          onChangeText={(text) => 
            this.setState({b: {...this.state.b,choice: text}}
          )}
        />
        <TextInput
          mode="flat"
          style={styles.inputStyle}
          label="C"
          value={this.state.c.choice}
          onChangeText={(text) => 
            this.setState({c: {...this.state.c,choice: text}}
          )}
        />
        <TextInput
          mode="flat"
          style={styles.inputStyle}
          label="D"
          value={this.state.d.choice}
          onChangeText={(text) => 
            this.setState({d: {...this.state.d,choice: text}}
          )}
        />
        <Button
        style={styles.button}
        onPress={() => this.storeQuestion()}
        mode="contained"
        >
         Tambah Pertanyaan
        </Button>            
        <Button
        style={styles.button}
        onPress={() => this.props.navigation.navigate('QuestionScreen')}
        mode="contained"
        >
         Daftar Pertanyaan
        </Button>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    paddingBottom: 30,
    paddingLeft: 35,
    paddingRight: 35,
    backgroundColor: '#fff'
  },
  inputStyle: {
    backgroundColor: 'white',
  },
  loginText: {
    color: '#4AB19D',
    marginTop: 15,
    textAlign: 'center'
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  title: {
    textAlign: "center",
    marginBottom: 50,
    fontSize: 24
  },
  button: {
    height: 50,
    marginTop: 15,
    borderRadius: 5,
    justifyContent:'center'
  }
})

export default AddUserScreen;