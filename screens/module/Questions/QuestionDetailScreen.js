import React, { Component } from 'react';
import { Alert, StyleSheet, ScrollView, View } from 'react-native';
import { TextInput, Button, ActivityIndicator, Colors } from 'react-native-paper';
import firebase from '../../../database/firebaseDb';

class QuestionDetailScreen extends Component {

  constructor() {
    super();
    this.state = {
      question: '',
      a: {
        choice: '',
      },
      b: {
        choice: ''
      },
      c: {
        choice: ''
      },
      d: {
        choice: ''
      },
      isLoading: true
    };
  }
 
  componentDidMount() {
    const dbRef = firebase.firestore().collection('questions').doc(this.props.route.params.userkey)
    dbRef.get().then((res) => {
      if (res.exists) {
        const data = res.data();
        this.setState({
          key: res.id,
          question: data.question,
          a: data.a,
          b: data.b,
          c: data.c,
          d: data.d,
          isLoading: false
        });
      } else {
        console.log("Document does not exist!");
      }
    });
  }

  inputValueUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  updateUser() {
    this.setState({
      isLoading: true,
    });
    const updateDBRef = firebase.firestore().collection('questions').doc(this.state.key);
    updateDBRef.set({
      question: this.state.question,
      a: this.state.a,
      b: this.state.b,
      c: this.state.c,
      d: this.state.d,
    }).then((docRef) => {
      this.setState({
        key: '',
        question: '',
        a: '',
        b: '',
        c: '',
        d: '',
        isLoading: false,
      });
      this.props.navigation.navigate('QuestionScreen');
    })
    .catch((error) => {
      console.error("Error: ", error);
      this.setState({
        isLoading: false,
      });
    });
  }

  deleteUser() {
    const dbRef = firebase.firestore().collection('questions').doc(this.props.route.params.userkey)
      dbRef.delete().then((res) => {
          console.log('Item removed from database')
          this.props.navigation.navigate('QuestionScreen');
      })
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" style={styles.preloader} animating={true} color={Colors.blue400} />
        </View>
      )
    }
    return (
      <ScrollView style={styles.container}>
        <View style={styles.inputGroup}>
          <TextInput
          mode="flat"
          style={styles.inputStyle}
          label="Pertanyaan"
          value={this.state.question}
          onChangeText={(val) => this.inputValueUpdate(val, 'question')}
        />
        </View>
        <View style={styles.inputGroup}>
        <TextInput
          mode="flat"
          style={styles.inputStyle}
          label="A"
          value={this.state.a.choice}
          onChangeText={(text) => 
            this.setState({a: {...this.state.a,choice: text}}
          )}
        />
        </View>
        <View style={styles.inputGroup}>
        <TextInput
          mode="flat"
          style={styles.inputStyle}
          label="B"
          value={this.state.b.choice}
          onChangeText={(text) => 
            this.setState({b: {...this.state.b,choice: text}}
          )}
        />
        </View>
        <View style={styles.inputGroup}>
        <TextInput
          mode="flat"
          style={styles.inputStyle}
          label="C"
          value={this.state.c.choice}
          onChangeText={(text) => 
            this.setState({c: {...this.state.c,choice: text}}
          )}
        />
        </View>
        <View style={styles.inputGroup}>
        <TextInput
          mode="flat"
          style={styles.inputStyle}
          label="D"
          value={this.state.d.choice}
          onChangeText={(text) => 
            this.setState({d: {...this.state.d,choice: text}}
          )}
        />
        </View>
        <View>
          <Button
            onPress={() => this.updateUser()}
            mode="contained"
            style={styles.button}
          >
            Update
          </Button>
          </View>
         <View>
          <Button
            onPress={() => this.deleteUser()}
            color={Colors.red500}
            mode="contained"
            style={styles.button}
          >
            Delete
          </Button>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
    backgroundColor: '#fff'
  },
  inputGroup: {
    padding: 0,
    marginBottom: 15,
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    marginBottom: 7,
    height: 50,
    marginTop: 15,
    borderRadius: 5,
    justifyContent:'center'
  },
  text: {
    marginRight: 20
  },
  inputStyle: {
    backgroundColor: 'white',
  },
})

export default QuestionDetailScreen;