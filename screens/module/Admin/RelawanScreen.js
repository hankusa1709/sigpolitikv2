// screens/UserScreen.js

import React, { Component } from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';
import { ActivityIndicator, Colors, Card, Searchbar } from 'react-native-paper';
import { FontAwesome5, Entypo } from '@expo/vector-icons';
import firebase from '../../../database/firebaseDb';

export default class RelawanScreen extends Component {

  constructor() {
    super();
    this.firestoreRef = firebase.firestore().collection('users');
    this.state = {
      isLoading: true,
      RelawanArr: [],
      RelawanTemp: [],
      search: ''
      // swipeablePanelActive: false
    };
  }

  componentDidMount() {
    this.unsubscribe = this.firestoreRef.onSnapshot(this.getCollection);
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  getCollection = (querySnapshot) => {
    const RelawanTemp = [];
    querySnapshot.forEach((res) => {
      const { name, nik, status, role } = res.data();
      if (role === "Relawan") {
        RelawanTemp.push({
          key: res.id,
          name,
          nik,
          status,
          role
        });
      }
    });
    this.setState({
      RelawanTemp,
      isLoading: false,
   });
   if (RelawanTemp) {
     let data = this.state.RelawanTemp
     this.setState({
       RelawanArr: data
     })
   }
  }

  updateSearch = search => {
    this.setState({ search })
    if (search === '') {
        this.setState({
            RelawanArr: [...this.state.RelawanTemp]
        });
        return;
    } else {
      let relawanFilter = this.state.RelawanTemp.filter(function(item){
        return item.nik.includes(search) || item.name.includes(search);
      })
      let result = relawanFilter.map(function({name, nik, status, role}){
          return {name, nik, status, role};
      });
      this.setState({
        RelawanArr: result
      })
    }
};

  render() {
    let { RelawanArr, isLoading, search } = this.state
    if(isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" style={styles.preloader} animating={true} color={Colors.blue400} />
        </View>
      )
    }    
    return (
      <ScrollView style={styles.container}>
        <Searchbar
          placeholder="Cari Relawan"
          onChangeText={this.updateSearch}
          value={search}
          style={styles.searchbar}
        />
          {
            RelawanArr.map((item, i) => {
              return (
                <View key={i}>
                  <Card style={ styles.card } key={i} onPress={() => {
                    this.props.navigation.navigate('RelawanDetailScreen', {
                      userkey: item.key
                    });
                  }}>
                    <Card.Title
                      title={ item.nik }
                      subtitle={ item.name }
                      left={(props) => item.status ?
                            <FontAwesome5 {...props} name="user-alt" size={30} color={Colors.grey800} style={{ marginLeft: '9%'}} /> :
                            <FontAwesome5 {...props} name="user-slash" size={30} color={Colors.red400} style={{ marginLeft: '9%'}} />
                        }
                      right={(props) => <Entypo {...props} name="chevron-small-right" size={25} style={{ marginRight: '3%' }} color={Colors.grey500} />}
                    />
                  </Card>
                </View>
              );
            })
          }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 10, 
    flex: 1,
    paddingBottom: 22,
    backgroundColor: 'white'
  },
  searchbar: {
    marginTop: 5,
    marginBottom: 20,
    width: '97%',
    alignSelf: 'center'
  },
  card: {
    borderRadius: 10,
    marginTop: '1%',
    marginBottom: '1%',
    width: '97%',
    alignSelf: 'center'
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  }
})