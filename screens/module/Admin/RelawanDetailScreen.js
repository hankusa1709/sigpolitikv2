import React, { Component } from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';
import { Provider as PaperProvider } from  'react-native-paper'
import { Title, Button, ActivityIndicator, Colors, Portal, Dialog, Paragraph } from 'react-native-paper';
import firebase from '../../../database/firebaseDb';
// import * as admin from 'firebase-admin';

class QuestionDetailScreen extends Component {

  constructor() {
    super();
    this.state = {
      id: '',
      nik: '',
      name: '',
      role: '',
      email: '',
      password: '',
      status: false,
      isLoading: true,
      visible: false,
      message: ''
    };
  }

  _showDialog = () => this.setState({ visible: true });

  _hideDialog = () => this.setState({ visible: false });
 
  componentDidMount() {
    let id = this.props.route.params.userkey
    const dbRef = firebase.firestore().collection('users').doc(id)
    dbRef.get().then((res) => {
      if (res.exists) {
        const data = res.data();
        this.setState({
          id: res.id,
          nik: data.nik,
          name: data.name,
          status: data.status,
          role: data.role,
          isLoading: false
        });
      } else {
        console.log("Document does not exist!");
      }
    });
  }

  inputValueUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  updateUser() {
      let status = !this.state.status
    this.setState({
      isLoading: true,
    });
    const updateDBRef = firebase.firestore().collection('users').doc(this.state.id);
    updateDBRef.set({
      name: this.state.name,
      nik: this.state.nik,
      role: this.state.role,
      status: status
    }).then((docRef) => {
      this.setState({
        id: '',
        nik: '',
        name: '',
        role: '',
        email: '',
        password: '',
        status: false,
        isLoading: false
      });
      this.props.navigation.navigate('RelawanScreen');
    })
    .catch((error) => {
      console.error("Error: ", error);
      this.setState({
        isLoading: false,
      });
    });
  }

  updateWarn() {
    let toBe = ''
    if (this.state.status) {
      toBe = 'Non-Aktifkan'
    } else {
      toBe = 'Aktifkan'
    }
      this.setState({
          message: "Akun Relawan Akan di " + toBe
      })
      this._showDialog()
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" style={styles.preloader} animating={true} color={Colors.blue400} />
        </View>
      )
    }
    return (
        <PaperProvider>
          <ScrollView style={styles.container}>
            <Portal>
                <Dialog
                  visible={this.state.visible}
                  onDismiss={this._hideDialog}>
                    <Dialog.Title>Yakin?</Dialog.Title>
                  <Dialog.Content>
                    <Paragraph>{ this.state.message }</Paragraph>
                  </Dialog.Content>
                  <Dialog.Actions>
                    <Button onPress={this._hideDialog}>Tidak</Button>
                    <Button onPress={() => this.updateUser()}>Iya</Button>
                  </Dialog.Actions>
                </Dialog>
            </Portal>
            <View>
              <View style={styles.detail}>
                <View style={styles.isiDetail1}>
                  <Title>NIK</Title>
                  <Title>Nama</Title>
                </View>
                <View style={styles.isiDetail2}>
                  <Title>:</Title>
                  <Title>:</Title>
                </View>
                <View style={styles.isiDetail3}>
                  <Title>{this.state.nik}</Title>
                  <Title>{this.state.name}</Title>
                </View>
              </View>
              <Button
                  onPress={() => this.updateWarn()}
                  mode="contained"
                  style={styles.button}
              >{ this.state.status ?
                  'Non-Aktifkan' :
                  'Aktifkan'
              } Relawan
              </Button>
            </View>
          </ScrollView>
        </PaperProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    padding: 35
  },
  inputGroup: {
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    marginBottom: 7,
    height: 50,
    marginTop: 15,
    borderRadius: 5,
    justifyContent:'center'
  },
  text: {
    marginRight: 20
  },
  name: {

  },
  nik: {

  },
  isiDetail1: {
    width: '20%' // is 50% of container width
  },
  isiDetail2: {
    width: '5%' // is 50% of container width
  },
  isiDetail3: {
    width: '75%' // is 50% of container width
  },
  detail: {
    // flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start' // if you want to fill rows left to right
  },
})

export default QuestionDetailScreen;