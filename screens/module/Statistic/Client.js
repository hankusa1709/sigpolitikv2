// screens/UserScreen.js

import React, { Component } from 'react';
import { StyleSheet, ScrollView, View, Animated } from 'react-native';
import { DataTable, ActivityIndicator, Colors, Text, Title, Surface } from 'react-native-paper';
import { ListItem } from 'react-native-elements'
import firebase from '../../../database/firebaseDb';
import LottieView from 'lottie-react-native';

class Statistic extends Component {

  constructor() {
    super();
    this.firestoreRef = firebase.firestore();
    this.firebaseData = firebase.firestore().collection('responden').orderBy('date_add', 'desc');
    this.state = {
      loadSize: true,
      size: 0,
      isLoading: true,
      StatisticTable: [],
      QuestionArr: [],
      Data: [],
      fadeAnim: new Animated.Value(0),
      Screen: new Animated.Value(0)
    };
  }

  fadeScreenIn = () => {
    this.setState({
      isLoading: false
    })
    Animated.timing(this.state.Screen, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true
    }).start()
  }

  fadeIn = () => {
    // Will change fadeAnim value to 1 in 5 seconds
    Animated.timing(this.state.fadeAnim, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true
    }).start();
  };

  fadeOut = () => {
    // Will change fadeAnim value to 0 in 5 seconds
    Animated.timing(this.state.fadeAnim, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true
    }).start(() => {
      this.fadeScreenIn()
    });
  };

  async componentDidMount() {
    this.fadeIn()
    await this.resetData()
    this.unsubscribe = this.firestoreRef.collection('questions').onSnapshot(this.getCollection);
    this.unsubscribe = this.firebaseData.onSnapshot(this.getSurveyed);
  }

  getSurveyed = async (querySnapshot) => {
    let size = querySnapshot.size
    this.setState({
      size: size,
      loadSize: false,
   });
  }

  resetData() {
    this.setState({
      QuestionArr: []
    })
  }

  async coba(QuestionArr) {
    this.setState({
      StatisticTable: []
    })
    await QuestionArr.forEach((res) => {
      const QuestionData = res
      let id = res.id
      if (id) {
        this.firestoreRef.collection('statistik').doc(id).collection('kota')
        .onSnapshot(async res => {
            const StatisticTable = []
            await res.forEach(res => {
              const { kota, a, b, c, d } = res.data();
              QuestionData.statistic.push({
                id: res.id,
                kota,
                a,
                b,
                c,
                d
              })
            })
            this.setState({
              StatisticTable,
            })
            if (this.state.StatisticTable) {
              this.fadeOut()
            }
          })
      }
      });
  }

  statisticData = (statisticSnapshot) => {
    let { QuestionArr } = this.state
    this.setState({
      isLoading: true
    })
    const StatisticTable = [];
    statisticSnapshot.forEach((res) => {
      const { kota, a, b, c, d } = res.data();
      StatisticTable.push({
        id: res.id,
        kota,
        a,
        b,
        c,
        d
      })
    })
  }

  componentWillUnmount(){
    this.unsubscribe();
    this.resetData()
  }

  getCollection = async (querySnapshot) => {
    this.setState({
      isLoading: true,
    })
    const QuestionArr = [];
    querySnapshot.forEach((res) => {
      const { question, a, b, c, d } = res.data();
      QuestionArr.push({
        id: res.id,
        question,
        a,
        b,
        c,
        d,
        statistic: []
      });
    });
    this.setState({
      QuestionArr
    });
    this.coba(QuestionArr)
  }

  render() {
    let { isLoading, fadeAnim, Screen, loadSize, size } = this.state

    if(isLoading){
      return(
        <View style={styles.preloader}>
          <Animated.View
            style={[
              {
                opacity: fadeAnim // Bind opacity to animated value
              }
            ]}
          >
            <LottieView style={{ alignSelf: 'center', width: '80%', elevation: 2 }} source = {require('../../../assets/dashboard.json')} autoPlay loop />
            <Text style={{ color: '#7f7f7f', fontSize: 15, textAlign: 'center' }}>Loading...</Text>
          </Animated.View>
        </View>
      )
    }    
    return (
      <ScrollView style={styles.container}>
        <Animated.View
            style={[
              styles.container,
              {
                opacity: Screen
              }
            ]}
          >
            <Title style={styles.title}>Update Terkini Survei Politik</Title>
            <Surface style={styles.surface}>
            { loadSize === false ?
                <Text style={styles.text}>{ size }</Text> :
                <ActivityIndicator size="small" style={styles.preloaderSize} animating={true} color={Colors.blue400} />
            }
            <Text style={styles.survei}>Tersurvei</Text>
            </Surface>
        {
            this.state.QuestionArr.map((item, i) => {
              return (
                <View
                  key={i}
                  style={{ marginBottom: 20, backgroundColor: 'white', elevation: 1 }}
                >
                  <ListItem
                    bottomDivider
                    title={(i + 1 ) + '. ' + item.question}
                  />
                  <DataTable style={{ backgroundColor: 'white', marginTop: 5, elevation: 1 }}>
                    <DataTable.Header>
                      <DataTable.Title>Kota/Kabupaten</DataTable.Title>
                      <DataTable.Title numeric>A</DataTable.Title>
                      <DataTable.Title numeric>B</DataTable.Title>
                      <DataTable.Title numeric>C</DataTable.Title>
                      <DataTable.Title numeric>D</DataTable.Title>
                    </DataTable.Header>
                  {
                    item.statistic.map((item, i) => {
                      return (
                      <DataTable.Row key={i}>
                        <DataTable.Cell>{item.kota}</DataTable.Cell>
                        <DataTable.Cell numeric>{item.a}</DataTable.Cell>
                        <DataTable.Cell numeric>{item.b}</DataTable.Cell>
                        <DataTable.Cell numeric>{item.c}</DataTable.Cell>
                        <DataTable.Cell numeric>{item.d}</DataTable.Cell>
                      </DataTable.Row>
                      )
                    })
                  }
                  </DataTable>
                </View>
              );
            })
          }
          </Animated.View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  surface: {
    marginTop: 5,
    marginBottom: 20,
    padding: 8,
    height: 180,
    width: 320,
    alignSelf: 'center',
    justifyContent: 'center',
    elevation: 4,
    borderRadius: 5
    },
  container: {
   flex: 1,
   paddingBottom: 22
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fafafa',
  },
  containerUp: {
    paddingBottom: 22,
    backgroundColor: "#FFF"
   },
  header: { height: 50, backgroundColor: '#537791' },
  text: { textAlign: 'center', fontWeight: '100' },
  dataWrapper: { marginTop: -1 },
  row: { height: 40, backgroundColor: '#E7E6E1' },
  title: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 23,
    fontSize: 20
  },
  text: {
    fontSize: 55,
    fontWeight: 'bold',
    marginLeft: 40
  },
  survei: {
    textAlign: "right",
    marginRight: 20,
    marginTop: 20
  }
})

export default Statistic;