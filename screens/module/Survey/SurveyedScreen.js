// screens/respondencreen.js

import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, View, PixelRatio, Animated, Image } from 'react-native';
import { Card, Title, Paragraph, Divider, TextInput, Button, Surface, Dialog, Portal, ActivityIndicator, Colors, Avatar } from 'react-native-paper';
import firebase from '../../../database/firebaseDb';
import { Ionicons } from '@expo/vector-icons';
import { firestore } from 'firebase';
import { Provider as PaperProvider } from 'react-native-paper';
import axios from 'axios';
import moment from 'moment'
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import uuid from 'react-native-uuid';
import Environment from '../../../database/environment';

var idLocale = require('moment/locale/id'); 
moment.locale('id', idLocale);

var FONT_BACK_LABEL = 60;

if (PixelRatio.get() <= 2) {
  FONT_BACK_LABEL = 200;
}

class SurveyedScreen extends Component {

  constructor() {
    super();
    this.firestoreRef = firebase.firestore().collection('responden');
    this.firebaseData = firebase.firestore().collection('responden').orderBy('date_add', 'desc');
    this.state = {
      scanning: false,
      image: '',
      error: '',
      nik: '3273151709010005',
      check: false,
      uploading: false,
      isLoading: true,
      isLoadingNIK: false,
      isLoadingCompletedNIK: true,
      userArr: [],
      res: '',
      total: '',
      size: 0,
      visible: false,
      loadSize: true,
      token: '',
      validateNik: false,
      ocrModal: false,
      gender: '',
      lahir: '',
      provinsi: '',
      kota: '',
      kecamatan: '',
      fadeAnim: new Animated.Value(0),
      fadeAnimNik: new Animated.Value(0),
      role: ''
    };
  }

  submitToGoogle(){
		this.setState({ scanning: true });
		let url = 'https://vision.googleapis.com/v1/images:annotate?key=' + Environment['GOOGLE_CLOUD_VISION_API_KEY']
		let { image } = this.state;
		let body = JSON.stringify({
			requests: [
				{
					features: [
						{ type: 'TEXT_DETECTION', },
						{ type: 'DOCUMENT_TEXT_DETECTION' },
					],
					image: {
						source: {
							imageUri: image
						}
					}
				}
			]
		});
		axios({
			method: 'POST',
			url: url,
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			data: body
		})
		.then(res => {
      let Extracted = res.data.responses[0].textAnnotations
      let filter = Extracted.filter(function(item){
        return item.description.includes("32")
      })
      let mapData = filter.map(function({description}){
        return description
      })
      let result = mapData.pop()
      if (result) {
        let nik = result
        this.setState({
          scanning: false,
          nik: nik,
          ocrModal: false,
          image: ''
        })
        this.fadeInEditNik()
      } else {
        this._showDialog()
        this.setState({
          scanning: false,
          ocrModal: false,
          image: '',
          error: 'Maaf, NIK Jawa Barat Tidak Ditemukan 😔'
        })
      }
		})
		.catch(err => {
      this._showDialog()
      this.setState({
        scanning: false,
        error: 'Maaf, ada suatu kesalahan, Coba Ulangi Lagi 😔'
      })
		})
		if (this.state.extractedNik) {
			this.StoreNik()
		}
}

  _takePhoto = async () => {
    if (!this.state.image) {
      let pickerResult = await ImagePicker.launchCameraAsync({
        allowsEditing: false
        // aspect: [4, 3]
      });
  
      this._handleImagePicked(pickerResult);
    } else {
      this._showDialogOcr()
    }
  };
  
  _handleImagePicked = async pickerResult => {
		try {
			this.setState({ uploading: true });

			if (!pickerResult.cancelled) {
				uploadUrl = await uploadImageAsync(pickerResult.uri);
        this.setState({ image: uploadUrl });
        this._showDialogOcr()
				// this.extractTextFromImage(image)
			}
		} catch (e) {
      this._showDialog()
      this.setState({
        error: 'Maaf, ada suatu kesalahan, Coba Ulangi Lagi 😔'
      })
		} finally {
			this.setState({ uploading: false });
		}
  };
  
  fadeInEditNik = () => {
    // Will change fadeAnim value to 1 in 5 seconds
    Animated.timing(this.state.fadeAnimNik, {
      toValue: 1,
      duration: 800,
      useNativeDriver: true
    }).start();
  };

  fadeIn = () => {
    // Will change fadeAnim value to 1 in 5 seconds
    Animated.timing(this.state.fadeAnim, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true
    }).start();
  };

  fadeOut = async () => {
    // Will change fadeAnim value to 0 in 5 seconds
    await Animated.timing(this.state.fadeAnim, {
      toValue: 0,
      duration: 500,
      useNativeDriver: true
    }).start(() => {
      this.setState({
        isLoading: false
      })
    });
  };

  _hideDialog = () => this.setState({ visible: false });
  _hideDialogNik = () => this.setState({ validateNik: false });
  _hideDialogOcr = () => this.setState({ ocrModal: false });
  
  _showDialog = () => this.setState({ visible: true });
  _showDialogNik = () => this.setState({ validateNik: true });
  _showDialogOcr = () => this.setState({ ocrModal: true });

  reset() {
    this.setState({
      nik: '',
      gender: '',
      lahir: '',
      provinsi: '',
      kota: '',
      kecamatan: ''
    })
    this._hideDialogNik()
  }

  storeNik() {
    if(this.state.nik === ''){
      this.setState({
        error: 'NIK tidak boleh kosong!'
      })
      this._showDialog()
    } else if (this.state.nik.length !== 16) {
      this.setState({
        error: 'NIK Harus 16 Digit'
      })
      this._showDialog()
    } else {
      this.setState({
        isLoading: true,
      });      
      this.firestoreRef.add({
        date_add: firebase.firestore.FieldValue.serverTimestamp(),
        nik: this.state.nik,
      }).then((res) => {
        this.setState({
          nik: '',
          status: false,
        });
        // this.countNik();
        // this.size();
      })
      .catch((err) => {
        console.error("Error found: ", err);
        this.setState({
          isLoading: false,
        });
      });
    }
  }

  countNik() {
    firestore()
    .collection('responden')
    // Filter
    .where('status', '==', true)
    .get()
    .then(querySnapshot => {
      this.setState({
        total: querySnapshot.size,
        isLoading: false,
      })
    })
    }

    survey() {
    let { nik, kota } = this.state
     this.props.navigation.navigate('SurveyScreen', {
       userkey: nik,
       kota: kota,
       surveyPassed: this.fromSurvey()
      })
    }

    validateNik() {
      this.setState({ isLoadingNIK: true })
      let { userArr, nik } = this.state
      let nikCollect = []
      userArr.forEach(res => {
        let EachNik = res.nik
        nikCollect.push(EachNik)
      })
      let a = nikCollect.includes(nik)
      if (a) {
        this._showDialog()
        this.setState({
          error: 'NIK Sudah Pernah Disurvei',
          nik: '',
          isLoadingNIK: false
        })
      } else {
        this.getLocation()
      }
    }

    getLocation() {
      let { nik } = this.state
      this.setState({ isLoadingNIK: true })
      if (nik === ''){
        this.setState({
          error: 'NIK tidak boleh kosong!',
          isLoadingNIK: false
        })
        this._showDialog()
      } else if (nik.length !== 16) {
        this.setState({
          error: 'NIK Harus 16 Digit',
          isLoadingNIK: false
        })
        this._showDialog()
      } else {
        let error = 'NIK Tidak Valid!'
        let provinsi = nik.substr(0, 2)
        let kota = nik.substr(0, 4)
        let gender = nik.substr(6, 2)
        let hari = nik.substr(6, 2)
        let bulan = nik.substr(8, 2)
        let tahun = nik.substr(10, 2)
        let thisYear = new Date().getFullYear().toString().substr(-2);
        if (gender > 40) {
          hari = hari - 40
          this.setState({
            gender: 'Wanita'
          })
        } else {
          this.setState({
            gender: 'Pria'
          })
        }
        if (tahun < thisYear) {
          tahun = '20' + tahun
        } else {
          tahun = '19' + tahun
        }
        if ((hari <= 31 || hari > 40 ) && bulan <= 12) {
          let tanggalLahir = hari + '/' + bulan + '/' + tahun;
          this.setState({
            lahir: tanggalLahir
          })
          let urlProvinsi = 'https://dev.farizdotid.com/api/daerahindonesia/provinsi/'+ provinsi
          let urlKota = 'https://dev.farizdotid.com/api/daerahindonesia/kota/'+ kota
          axios.get(urlProvinsi)
          .then(res => {
            let provinsi = res.data.nama
            this.setState({provinsi:provinsi})
            axios.get(urlKota)
          .then(res => {
            let kota = res.data.nama
            this.setState({kota:kota, isLoadingNIK: false})
            this._showDialogNik()
          })
          .catch(err => {
            this._showDialog()
            this.setState({
              error: error,
              isLoadingNIK: false
            })
          })
          })
          .catch(err => {
            this._showDialog()
            this.setState({
              error: error,
              isLoadingNIK: false
            })
          })
        } else {
          this.setState({
            error: error,
            isLoadingNIK: false
          })
          this._showDialog()
        }
      }
    }

    isiSurvey() {
      this.props.navigation.navigate('SurveyScreen', {
        userkey: this.state.nik
      });
    }

  updateInputVal = (nik) => {
    let newText = '';
    let numbers = '0123456789';

    for (var i=0; i < nik.length; i++) {
        if(numbers.indexOf(nik[i]) > -1 ) {
            newText = newText + nik[i];
        }
        else {
            // your call back function
        this._showDialog()
        this.setState({
          error: 'Masukkan Hanya Angka Saja!'
        })
        }
    }
    this.setState({ nik: newText });
  }

  fromSurvey() {
    this.reset();
    this._hideDialogNik();
  }

  async componentDidMount() {
      await Permissions.askAsync(Permissions.CAMERA_ROLL);
      await Permissions.askAsync(Permissions.CAMERA);
      this.fadeIn()
      this.fadeInEditNik()
      this.unsubscribe = this.firebaseData.onSnapshot(this.getCollection);
      this.loadRole()
    }
    
    componentWillUnmount(){
    this.countNik();
    this.unsubscribe();
  }

  loadRole() {
    var user = firebase.auth().currentUser
      if (user) {
        this.setState({
        })
        let id = user.uid   
        if (id) {
            let ref = firebase.firestore().collection('users').doc(id)
            ref.get().then((res) => {
              let data = res.data()
              let { role } = data
              this.setState({
                role: role
              })
            })
        }
      }
  }

  resetImage() {
    this.setState({
      image: ''
    })
  }

  getCollection = async (querySnapshot) => {
    let size = querySnapshot.size
    const userArr = [];
    querySnapshot.forEach((res) => {
      const { nik, status, date_add } = res.data();
      userArr.push({
        key: res.id,
        res,
        nik,
        status,
        date_add
      });
    });
    await this.fadeOut()
    this.setState({
      size: size,
      userArr,
      loadSize: false,
      isLoadingCompletedNIK: false,
   });
  }

  check() {
    this.setState({
      check: true
    })
  }

  render() {
    let { image, ocrModal, isLoading, fadeAnim, uploading, visible, error, validateNik, provinsi, kota,
      lahir, gender, loadSize, size, nik, fadeAnimNik, isLoadingNIK, userArr, isLoadingCompletedNIK,
      scanning, role } = this.state

    if(isLoading){
      return(
        <View style={styles.preloader}>
          <Animated.View
            style={[
              {
                opacity: fadeAnim // Bind opacity to animated value
              }
            ]}
          >
            <Image style={{ width: 300, height: 300 }} source={require('../../../assets/logo.png')} />
            <Text style={{ color: '#7f7f7f', fontSize: 15, textAlign: 'center' }}>Loading...</Text>
          </Animated.View>
        </View>
      )
    }    
    return (
      <PaperProvider>
      <Portal>
        <Dialog
          visible={ocrModal}
          onDismiss={this._hideDialogOcr}>
          <Dialog.Title>Scan E-KTP</Dialog.Title>
          <Dialog.Content>
            { uploading ?
              <View>
                <ActivityIndicator size="small" animating={true} color={Colors.blue400} />
                <Paragraph>Uploading...</Paragraph>
              </View> :
               scanning ?
               <View>
                <ActivityIndicator size="small" animating={true} color={Colors.green400} />
                <Paragraph>Scanning...</Paragraph>
              </View> :
              image ?
              <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <Paragraph>Uploaded</Paragraph><Ionicons style={{ marginLeft: 10 }} name="ios-checkmark" size={28} color={Colors.blue400} ></Ionicons>
              </View> :
               <Button
               style={styles.cameraButton}
               onPress={this._takePhoto}
                  mode="contained"
                  >
                  Foto KTP
                </Button>
            }
          </Dialog.Content>
          { image ?
            <Dialog.Actions>
              <Button onPress={() => this.resetImage()} color={Colors.red400} disabled={scanning}>Reset</Button>
              <Button onPress={() => this.submitToGoogle()} disabled={scanning}>GET NIK</Button>
            </Dialog.Actions> :
            null
          }
        </Dialog>
      </Portal>
      <Portal>
        <Dialog
          visible={visible}
          onDismiss={this._hideDialog}>
          <Dialog.Title>Error!</Dialog.Title>
          <Dialog.Content>
            <Paragraph> { error } </Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={() => this._hideDialog()}>Ok</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      <Portal>
        <Dialog
          visible={ validateNik }
          onDismiss={this._hideDialogNik}>
          <Dialog.Title>NIK</Dialog.Title>
          <Dialog.Content style={styles.detail}>
            <View style={styles.isiDetail1}>
              <Paragraph>Provinsi</Paragraph>
              <Paragraph>Kabupaten/Kota</Paragraph>
              <Paragraph>Tanggal Lahir</Paragraph>
              <Paragraph>Jenis Kelamin</Paragraph>
            </View>
            <View style={styles.isiDetail2}>
              <Paragraph>:</Paragraph>
              <Paragraph>:</Paragraph>
              <Paragraph>:</Paragraph>
              <Paragraph>:</Paragraph>
            </View>
            <View style={styles.isiDetail3}>
              <Paragraph>{provinsi}</Paragraph>
              <Paragraph>{kota}</Paragraph>
              <Paragraph>{lahir}</Paragraph>
              <Paragraph>{gender}</Paragraph>
            </View>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={() => this.survey()}>Survei</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      <ScrollView style={styles.container}>
        <Title style={styles.title}>Update Terkini Survei Politik</Title>
        <Surface style={styles.surface}>
          { loadSize === false ?
            <Text style={styles.text}>{ size }</Text> :
            <ActivityIndicator size="small" style={styles.preloaderSize} animating={true} color={Colors.blue400} />
          }
          <Text style={styles.survei}>Tersurvei</Text>
        </Surface>
        { role === 'Admin' ?
          <Surface style={styles.camera}>
            <Button
              style={styles.cameraButton}
              onPress={this._showDialogOcr}
              mode="contained"
              >
              Scan E-KTP
            </Button>
          </Surface> :
          null
        }
        { nik ?
        <Animated.View
        style={[
          {
            opacity: fadeAnimNik // Bind opacity to animated value
          }
        ]}
      >
        <Surface style={styles.card}>
          <TextInput
            style={styles.inputStyle}
            keyboardType = 'numeric'
            label="NIK"
            value={nik}
            onChangeText={(nik) => this.updateInputVal(nik)}
            maxLength={16}
            />
        { isLoadingNIK === false ?
          <Button
            style={styles.button}
            onPress={() => this.validateNik()}
            mode="contained"
            >
            Input
          </Button> :
          <ActivityIndicator style={styles.button} size="small" animating={true} color={Colors.blue400} />
        }
        </Surface> 
        </Animated.View> :
        null
        }
        <Divider />
          {
            userArr.map((item, i) => {
              return (
                <Card 
                  key={i}
                >
                  { isLoadingCompletedNIK === false ?
                      <Card.Title
                        title={ item.nik }
                        subtitle={item.date_add ? 'Tersurvei Pada Tanggal ' + moment(item.date_add.toDate()).format('LL') : 'Tidak Diketahui'}
                        left={(props) => <Avatar.Icon {...props} icon="fingerprint" backgroundColor={Colors.blue400} />}
                      /> :
                      <Card.Content style={{ height: 70 }}>
                        <ActivityIndicator size="small" style={styles.preloader} animating={true} color={Colors.blue400} />
                      </Card.Content>
                  }
                    <Divider />
                </Card>
              );
            })
          }
        {/* </Surface> */}
          {/* <CameraRN /> */}
      </ScrollView>
      </PaperProvider>
    );
  }
}

async function uploadImageAsync(uri) {
  const blob = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
      resolve(xhr.response);
    };
    xhr.onerror = function(e) {
      console.log(e);
      reject(new TypeError('Network request failed'));
    };
    xhr.responseType = 'blob';
    xhr.open('GET', uri, true);
    xhr.send(null);
  });

  const ref = firebase
    .storage()
    .ref()
    .child(uuid.v4());
  const snapshot = await ref.put(blob);

  blob.close();

  return snapshot.ref.getDownloadURL();
  
}

const styles = StyleSheet.create({
  detail: {
    // flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start' // if you want to fill rows left to right
  },
  isiDetail1: {
    width: '45%' // is 50% of container width
  },
  isiDetail2: {
    width: '5%' // is 50% of container width
  },
  isiDetail3: {
    width: '50%' // is 50% of container width
  },
  container: {
   paddingBottom: 22,
   backgroundColor: "#FFF"
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#FFF"
  },
  preloaderSize: {
    left: 0,
    right: '20%',
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#FFF"
  },
  done : {
    color: '#E33A3A',
    justifyContent: "center"
  },
  unDone: {
    color: '#E33A3A',
    justifyContent: "center"
  },
  card: {
    marginBottom: 20,
    padding: 3,
    height: 120,
    width: 320,
    alignSelf: 'center',
    justifyContent: 'center',
    elevation: 4,
    borderRadius: 5,
    flexDirection: "row"
  },
  list: {
    marginBottom: 20,
    padding: 3,
    height: 120,
    width: 400,
    alignSelf: 'center',
    justifyContent: 'center',
    elevation: 4,
    borderRadius: 5,
    flexDirection: "row"
  },
  camera: {
    marginBottom: 20,
    padding: 3,
    height: 120,
    width: 320,
    alignSelf: 'center',
    justifyContent: 'center',
    elevation: 4,
    borderRadius: 5,
    flexDirection: "row"
  },
  surface: {
    marginTop: 5,
    marginBottom: 20,
    padding: 8,
    height: 180,
    width: 320,
    alignSelf: 'center',
    justifyContent: 'center',
    elevation: 4,
    borderRadius: 5
  },
  inputStyle: {
    backgroundColor: 'white',
    width: 180,
    height: 100,
    marginBottom: 30,
    marginRight: 25,
  },
  text: {
    fontSize: 55,
    fontWeight: 'bold',
    marginLeft: 40
  },
  button: {
    height: 50,
    width: 80,
    alignSelf:"center",
    borderRadius: 5,
    justifyContent:'center',
    marginTop: 15
  },
  cameraButton: {
    height: 50,
    width: 200,
    alignSelf:"center",
    borderRadius: 5,
    justifyContent:'center'
  },
  title: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 23,
    fontSize: 20
  },
  actions: {
    alignSelf:"center",
  },
  no: {
    width: '50%'
  },
  survei: {
    textAlign: "right",
    marginRight: 20,
    marginTop: 20
  }
})

export default SurveyedScreen;