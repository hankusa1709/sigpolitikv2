// screens/UserScreen.js

import React, { Component } from 'react'
import { StyleSheet, ScrollView, View, Text } from 'react-native'
import { Provider as PaperProvider, Subheading } from 'react-native-paper'
import { Card, Title, Paragraph, Divider, Button, Dialog, Portal, TouchableRipple, ActivityIndicator, Colors, IconButton, TextInput, Caption, Headline } from 'react-native-paper'
import firebase from '../../../database/firebaseDb'
import { firestore } from 'firebase';
import { ButtonContainer } from "../../components/Button";
import uuid from 'react-native-uuid';

class SurveyScreen extends Component {
  constructor() {
    super();
    this.userRef = firebase.firestore().collection('users');
    this.firestoreRef = firebase.firestore().collection('questions');
    this.respondenRef = firebase.firestore().collection('responden')
    this.state = {
      label: '',
      kota: '',
      key: '',
      check: false,
      isLoading: true,
      QuestionArr: [],
      sugestionArr: [],
      nik: '',
      visible: false,
      selesai: false,
      pressStatus: true,
      activeIndex: 1,
      index: 0,
      other: '',
      error: false,
      size: 0,
      id_question: '',
      id_parent: '',
      choice: '',
      a: 0,
      b: 0,
      c: 0,
      d: 0
    }
  }

  _showDialogError = () => this.setState({ error: true })

  _hideDialog = () => this.setState({ visible: false, selesai: false, error: false })

  _increaseIndex = () => this.setState({ index: this.state.index + 1, activeIndex: this.state.activeIndex + 1 })
  _decreaseIndex = () => this.setState({ index: this.state.index - 1, activeIndex: this.state.activeIndex - 1 })

  cancel() {
    this.setState({
      visible: true
    })
  }

  loadParams() {
    let { userkey, kota } = this.props.route.params
    this.setState({
      nik: userkey,
      kota: kota
    })
  }

  back() {
    this.props.navigation.navigate('SurveyedScreen')
  }

  prev() {
    this._decreaseIndex()
    this.resetChoice()
    this.choiceLoad()
    this.initialStatistic()
  }

  async initialStatistic() {
    let { QuestionArr, index, id_question, nik } = this.state
    const question = QuestionArr[index]
    let idKota = nik.substr(0, 4)
    let idQuest = question.res.id
    if (idQuest) {
      let statistic = firebase.firestore().collection('statistik').doc(idQuest).collection('kota').doc(idKota)
      statistic.get().then((res) => {
        if (res.exists) {
          const data = res.data();
          this.setState({
            key: res.id,
            kota: data.kota,
            a: data.a,
            b: data.b,
            c: data.c,
            d: data.d,
            isLoading: false
          });
        } else {
          console.log("Document does not exist!");
        }
      });
    }
  }

  resetChoice() {
    this.setState({
      choice: ''
    })
  }

  inputValueUpdate = (val, prop) => {
    const state = this.state;
    state[prop] + 1;
    this.setState(state);
  }

  incrementChoice() {
    let { label } = this.state
    const increment = firebase.firestore.FieldValue.increment(1);
    let bas = label
    let sososo = 'ab'
    this.setState({
      [label]: sososo
    })
  }

  increaseChoice() {
    let { id_question, label, nik, a, b, c, d } = this.state
    let idKota = nik.substr(0, 4)
    let ya = this.state[label] + 1
    this.state[label] = ya
    this.setState({
      [label]: ya
    })
    let statistic = firebase.firestore().collection('statistik').doc(id_question).collection('kota').doc(idKota)
    statistic.set({
      kota: this.state.kota,
      a: this.state.a,
      b: this.state.b,
      c: this.state.c,
      d: this.state.d
    })
    .then(res => {
      this.setState({
        kota: this.state.kota,
        a: 0,
        b: 0,
        c: 0,
        d: 0,
        isLoading: false
      })
    })
    .catch(err => {
      console.log('error', err)
    })
  }

  choiceLoad() {
    let { id_question, id_parent } = this.state
    if (id_parent) {
      const dbRef = firebase.firestore().collection('responden').doc(id_parent).collection('answer').doc(id_question)
      dbRef.get().then((res) => {
        if (res.exists) {
          const data = res.data()
          this.setState({
            choice: data.choice
          })
        } else {
          console.log("This Is Error")
        }
      })
    }
  }

  storeNik() {
    let { nik, kota } = this.state
    const dbRef = firebase.firestore().collection('responden').doc(this.state.id_parent)
    this.setState({
      isLoading: true,
    });      
    dbRef.set({
      date_add: firebase.firestore.FieldValue.serverTimestamp(),
      nik: nik,
      kota: kota
    }).then((res) => {
      this.setState({
        nik: '',
        id_parent: '',
        id_question: '',
        choice: '',
      });
    })
    .catch((err) => {
      console.error("Error found: ", err);
      this.setState({
        isLoading: false,
      });
    });
  }

  createId() {
    let id = uuid.v4()
    this.setState({
      id_parent: id
    })
    console.log('idperant', id)
  }

  next() {
    let { id_parent, id_question, choice, other } = this.state
    this.choiceLoad()
    this.setState({
      isLoading: true
    })
    if((choice || other) && id_parent) {
      const surveyData = firebase.firestore().collection('responden').doc(id_parent).collection('answer').doc(id_question)
      this._increaseIndex()
      let key = choice ? 'choice' : 'other'
      surveyData.set({
        [key]: this.state[key]
      }).then((res) =>{
        this.resetChoice()
        this.increaseChoice()
        this.setState({
            choice: '',
            other: ''
          })
        }).catch((err) =>{
          console.log('err', err)
          this._showDialogError()
        })
    } else {
      this._showDialogError()
    }
  }

  finish() {
    let { id_parent, id_question, choice } = this.state
    if(choice && id_parent) {
      const surveyData = firebase.firestore().collection('responden').doc(id_parent).collection('answer').doc(id_question)
      surveyData.set({
        choice: this.state.choice
      }).then((res) =>{
        this.resetChoice()
        this.increaseChoice()
        this._increaseIndex()
          this.setState({
            choice: ''
          })
        })
    } else {
      this._showDialogError()
    }
    this.storeNik();
    this.props.navigation.navigate('SurveyedScreen')
  }

  updateInputVal = (val, prop) => {
    let current = this.state.index
    let question = this.state.QuestionArr[current]

    this.setState({
      [prop]: val,
      choice: '',
      label: '',
      id_question: question.res.id,
      pressStatus: false
    })
    this.initialStatistic()
  }

  selected(data) {
    let current = this.state.index
    let question = this.state.QuestionArr[current]

    this.setState({
      choice: data.choice,
      other: '',
      label: data.label,
      id_question: question.res.id,
      pressStatus: false
    })
    this.initialStatistic()
  }

  selesai() {
    this.setState({
      isLoading: true,
    })
    const updateDBRef = firebase.firestore().collection('users').doc(this.state.key)
    updateDBRef.set({
      nik: this.state.nik,
      status: true,
    }).then((docRef) => {
      this.setState({
        selesai: false,
        isLoading: false,
      })
      const {params} = this.props.navigation.state;
      params.surveyPassed
      this.props.navigation.navigate('SurveyedScreen')
    })
    .catch((error) => {
      console.error("Error: ", error);
      this.setState({
        isLoading: false,
      })
    })
  }

  async componentDidMount() {
    await this.createId();
    this.size();
    this.loadParams()
    this.unsubscribe = await this.firestoreRef.onSnapshot(this.getCollection)
    this.unsubscribe = await this.respondenRef.onSnapshot(this.getResponden);
    this.initialStatistic()
  }

  componentWillUnmount(){
    this.loadParams()
    this.unsubscribe()
  }

  getResponden = async (querySnapshot) => {
    let sugestionArr = []
    querySnapshot.forEach((res) => {
      this.respondenRef.doc(res.id).collection('answer').doc(id_question).get()
      .then(res => {
        console.log('res nya', res)
        let { other } = res.data()
        sugestionArr.push(other)
      })
    })
    console.log('sugest arr', sugestionArr)
    this.setState({
      sugestionArr
    })
  }

  getCollection = (querySnapshot) => {
    const QuestionArr = []
    querySnapshot.forEach((res) => {
      const { id, question, a, b, c, d } = res.data()
      QuestionArr.push({
        key: res.id,
        res,
        id,
        question,
        a,
        b,
        c,
        d,
      })
    })
    this.setState({
      QuestionArr,
      isLoading: false,
   })
  }

  size() {
    this.setState({
      loadSize: true,
    });
    firestore()
    .collection('questions')
    // Filter
    .get()
    .then(querySnapshot => {
      this.setState({
        size: querySnapshot.size,
        // loadSize: false,
      })
    })
  }

  check() {
    this.setState({
      check: true
    })
  }
  
  render() {
    let { QuestionArr, index, visible, selesai, error } = this.state
    const question = QuestionArr[index]

    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" style={styles.preloader} animating={true} color={Colors.blue400} />
        </View>
      )
    }
    return (
      <View style={{flex:1}}>
        <Portal>
          <Dialog
            visible={visible}
            onDismiss={this._hideDialog}>
            <Dialog.Title>Apakah Anda Yakin?</Dialog.Title>
            <Dialog.Content>
              <Paragraph>Seluruh Data Tidak akan tersimpan</Paragraph>
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={() => this._hideDialog()}>Tidak</Button>
              <Button onPress={() => this.back()}>Iya</Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
        <Portal>
          <Dialog
            visible={selesai}
            onDismiss={this._hideDialog}>
            <Dialog.Title>Apakah Anda Yakin?</Dialog.Title>
            <Dialog.Content>
              <Paragraph>Seluruh Data Akan Tersimpan dan Tak Dapat Diubah</Paragraph>
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={() => this._hideDialog()}>Tidak</Button>
              <Button onPress={() => this.selesai()}>Iya</Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
        <Portal>
          <Dialog
            visible={error}
            onDismiss={this._hideDialog}>
            <Dialog.Title>Jawaban Belum Dipilih</Dialog.Title>
            <Dialog.Content>
              <Paragraph>Dimohon Untuk Memilih Salah Satu Jawaban Sebelum Lanjut</Paragraph>
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={() => this._hideDialog()}>OKE</Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
          <Card>
            <Card.Content>
              <Card.Title
                title={ 'NIK : ' + this.state.nik }
              />
            </Card.Content>
          </Card>
          <ScrollView style={styles.container}>
          <Card>
              <Card.Content>
                <Title>{ question && question.question }</Title>
                  <ButtonContainer>
                    <TouchableRipple
                      style={
                        this.state.choice === question.a.choice
                            ? styles.buttonPress
                            : styles.button
                        }
                      onPress={() => this.selected(question.a)}
                      rippleColor="#F07536"
                    >
                      <Text style={styles.text}>{ question && question.a.choice }</Text>
                    </TouchableRipple>
                    <TouchableRipple
                      style={
                        this.state.choice === question.b.choice
                            ? styles.buttonPress
                            : styles.button
                        }
                      onPress={() => this.selected(question.b)}
                      rippleColor="#F07536"
                    >
                      <Text style={styles.text}>{ question && question.b.choice }</Text>
                    </TouchableRipple>
                    <TouchableRipple
                      style={
                        this.state.choice === question.c.choice
                            ? styles.buttonPress
                            : styles.button
                        }
                      onPress={() => this.selected(question.c)}
                      rippleColor="#F07536"
                    >
                      <Text style={styles.text}>{ question && question.c.choice }</Text>
                    </TouchableRipple>
                    <TouchableRipple
                      style={
                        this.state.choice === question.d.choice
                            ? styles.buttonPress
                            : styles.button
                        }
                      onPress={() => this.selected(question.d)}
                      rippleColor="#F07536"
                    >
                      <Text style={styles.text}>{ question && question.d.choice }</Text>
                    </TouchableRipple>
                </ButtonContainer>
              </Card.Content>
          </Card>
          <Card>
            <TextInput
              style={{ width: "80%", alignSelf: 'center', backgroundColor: 'white'}}
              label='Lainnya'
              mode='flat'
              value={this.state.other}
              onChangeText={(val) => this.updateInputVal(val, 'other')}
            />
            <Text style={{ marginTop: "5%", marginBottom: "2%", marginLeft: "5%" }}>Jawaban Lainnya dari orang lain :</Text>
            <Caption style={{ marginLeft: "7%" }}>Tingkat Kriminalitas</Caption>
            <Caption style={{ marginLeft: "7%" }}>Kurangnya Pohon</Caption>
            <Caption style={{ marginLeft: "7%" }}>Polusi Udara</Caption>
          </Card>
        </ScrollView>
        {/* <View>
        </View> */}
        <View>
          <Card>
          <Card.Actions style={{flexDirection: 'row'}}>
            { this.state.index === 0 ?
              null :
              <IconButton
                icon="chevron-left"
                style={styles.buttonPrev}
                color={Colors.blue500}
                size={30}
                onPress={() => this.prev()}
              />
            }
            { this.state.activeIndex === this.state.size ?
              <Button
                style={styles.buttonNext}
                onPress={() => this.finish()}
              >
                Selesai
              </Button> :
            <IconButton
              icon="chevron-right"
              style={styles.buttonNext}
              color={Colors.blue500}
              size={30}
              onPress={() => this.next()}
            />
            }
            </Card.Actions>
            <Card.Content>
              <Title style={{textAlign:'center'}}>{this.state.activeIndex} dari {this.state.size} Survei</Title>
            </Card.Content>
          </Card>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1,
    paddingBottom: 10
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#FFF"
  },
  buttonPrev: {
    marginHorizontal: 20,
    left:25,
    marginRight: 'auto'
  },
  buttonNext: {
    marginHorizontal: 20,
    right:25,
    marginLeft: 'auto'
  },
  answer: {
    flex: 1,
    flexDirection: 'row'
  },
  value: {
    alignItems: 'center',
    paddingTop: 4
  },
  button: {
    backgroundColor: "#8819E6",
    borderRadius: 10,
    paddingVertical: 15,
    alignItems: "center",
    justifyContent: "center",
    width: "49%",
    marginTop: 20,
    elevation: 2
  },
  buttonPress: {
    backgroundColor: "#E67919",
    borderRadius: 10,
    paddingVertical: 15,
    alignItems: "center",
    justifyContent: "center",
    width: "49%",
    marginTop: 20,
    elevation: 2
  },
  text: {
    color: "#fff",
    fontSize: 20,
    textAlign: "center",
    margin: 6
  },
  buttonContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 20,
    justifyContent: "space-between"
  }
})

export default SurveyScreen