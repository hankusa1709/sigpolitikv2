import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import {
    Title,
    Caption,
    Drawer,
    Colors
} from 'react-native-paper';
import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer';

import firebase from '../../database/firebaseDb';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

function User(props) {
    let ref = firebase.auth().currentUser
    if (ref != null) {
        let name = ref.displayName
        return <Title style={styles.title}>{name}</Title>
    }
}

function UserDetail(props) {

    const [role, setRole] = useState('');
    const [name, setName] = useState('');

    let getUser = firebase.auth().currentUser
    let id = getUser.uid
    if (id) {
        let ref = firebase.firestore().collection('users').doc(id)
        ref.get().then((res) => {
            const data = res.data()
            setRole(data.role)
            setName(data.name)
        })
    }
    return (
        <View style={{marginLeft:15, flexDirection:'column'}}>
            <Title style={styles.title}>{ name }</Title>
            <Caption style={styles.caption}>{ role }</Caption>
        </View>
    )
}

export function DrawerContent(props) {

    const SignOut = () => {
        firebase.auth().signOut().then(() => {
            props.navigation.navigate('LoginScreen')
        })
        .catch(error => {
            console.log('err', error)
        }) 
    }

    return(
        <View style={{flex:1}}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        <View style={{flexDirection:'row',marginTop: 15}}>
                        <Icon 
                                name="account-outline" 
                                color={Colors.blue400}
                                size={50}
                                />
                                <UserDetail />
                        </View>

                    </View>

                    {/* <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="home-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Home"
                            onPress={() => {props.navigation.navigate('Home')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="account-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Profile"
                            onPress={() => {props.navigation.navigate('Profile')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="bookmark-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Bookmarks"
                            onPress={() => {props.navigation.navigate('BookmarkScreen')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="settings-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Settings"
                            onPress={() => {props.navigation.navigate('SettingsScreen')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="account-check-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Support"
                            onPress={() => {props.navigation.navigate('SupportScreen')}}
                        />
                    </Drawer.Section> */}
                </View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                <DrawerItem 
                    icon={({color, size}) => (
                        <Icon 
                        name="exit-to-app" 
                        color={color}
                        size={size}
                        />
                    )}
                    label="Sign Out"
                    onPress={SignOut}
                />
            </Drawer.Section>
        </View>
    );
}

const styles = StyleSheet.create({
    drawerContent: {
      flex: 1,
    },
    userInfoSection: {
      paddingLeft: 20,
    },
    title: {
      fontSize: 16,
      marginTop: 3,
      fontWeight: 'bold',
    },
    caption: {
      fontSize: 14,
      lineHeight: 14,
    },
    row: {
      marginTop: 20,
      flexDirection: 'row',
      alignItems: 'center',
    },
    section: {
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 15,
    },
    paragraph: {
      fontWeight: 'bold',
      marginRight: 3,
    },
    drawerSection: {
      marginTop: 15,
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 12,
      paddingHorizontal: 16,
    },
  });