// components/login.js

import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Text, TextInput, Title, ActivityIndicator, Colors, Paragraph, Dialog, Portal, TouchableRipple } from 'react-native-paper';
import firebase from '../../database/firebaseDb';


class LoginScreen extends Component {
  
  constructor() {
    super();
    this.userRef = firebase.firestore().collection('users');
    this.state = {
      email: '', 
      password: '',
      isLoading: false,
      showToast: false,
      message: '',
      messageTitle: '',
      disabledInput: false,
      signedUp: false,
    }
  }

  _showDialog = () => this.setState({ visible: true });

  _hideDialog = () => this.setState({ visible: false });

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  userLogin = () => {
    if(this.state.email === '' || this.state.password === '') {
      this.setState({
        message: 'Dimohon Untuk Melengkapi Form'
      })
      this._showDialog()
    } else {
      this.setState({
        isLoading: true,
        disabledInput: true
      })
      firebase
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then((res) => {
        let { uid } = res.user
        let ref = this.userRef.doc(uid)
        ref.get().then((res) => {
          let data = res.data()
          let { status } = data
          if (status) {
            this.setState({
              isLoading: false,
              disabledInput: false,
              email: '', 
              password: ''
            })
            this.props.navigation.navigate('Draw')
          } else {
            this.setState({
              message: 'Akun anda dinonaktifkan, silahkan hubungi Admin.',
              messageTitle: 'Akses Dilarang!',
              isLoading: false,
              disabledInput: false,
              email: '', 
              password: ''
            })
            this._showDialog()
          }
        })
      })
      .catch((error) => {
        this.setState({
          message: 'Email atau Password Salah!',
          messageTitle: 'Error',
          isLoading: false,
          disabledInput: false
        })
        this._showDialog()
      })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Portal>
          <Dialog
             visible={this.state.visible}
             onDismiss={this._hideDialog}>
            <Dialog.Title>{ this.state.messageTitle }</Dialog.Title>
            <Dialog.Content>
              <Paragraph>{ this.state.message }</Paragraph>
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={this._hideDialog}>Ok</Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
        <Title style={styles.title}>Login</Title>
        <TextInput
          disabled={this.state.disabledInput}
          style={styles.inputStyle}
          label="Email"
          value={this.state.email}
          onChangeText={(val) => this.updateInputVal(val, 'email')}
        />
        <TextInput
          disabled={this.state.disabledInput}
          style={styles.inputStyle}
          label="Password"
          value={this.state.password}
          onChangeText={(val) => this.updateInputVal(val, 'password')}
          secureTextEntry={true}
        />
        { this.state.isLoading === true ?
          <View style={{ marginTop: 20 }}>
            <Text style={{ color: '#7f7f7f', fontSize: 15, textAlign: 'center', marginBottom: 10 }}>Signing In...</Text>
            <ActivityIndicator size="small" animating={true} color={Colors.blue400} />
          </View> :
          <View style={{ marginTop: 20 }}>
            <Button
              style={styles.button}
              onPress={() => this.userLogin()}
              mode="contained"
            >
              Login
            </Button>
            <TouchableRipple
                onPress={() => this.props.navigation.navigate('SignUpScreen')}
                style={{ marginTop:25, height: 25, justifyContent: 'center' }}
              >
                <Text style={styles.loginText}>
                  Already Registered? Click here to Sign Up
                </Text>  
              </TouchableRipple>
          </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: 35,
    backgroundColor: '#fff'
  },
  inputStyle: {
    backgroundColor: 'white',
    marginBottom: 15,
  },
  loginText: {
    color: '#4AB19D',
    textAlign: 'center'
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  title: {
    textAlign: "center",
    marginBottom: 50,
    fontSize: 24
  },
  button: {
    height: 50,
    borderRadius: 5,
    justifyContent:'center'
  }
});
export default LoginScreen;