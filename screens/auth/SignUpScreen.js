import React, { Component } from 'react';
import { StyleSheet, View, Alert } from 'react-native';
import { Button, Text, TextInput, Title, ActivityIndicator, Colors, Portal, Dialog, Paragraph, TouchableRipple } from 'react-native-paper';
import { Provider as PaperProvider } from 'react-native-paper';
import firebase from '../../database/firebaseDb';


class SignUpScreen extends Component {
  
  constructor() {
    super();
    this.dbRef = firebase.firestore().collection('users');
    this.state = {
      id_user: '',
      nik: '',
      name: '',
      email: '', 
      password: '',
      isLoading: false,
      disabledInput: false,
      visible: false,
      error: '',
    }
  }

  _hideDialog = () => this.setState({ visible: false });

  _showDialog = () => this.setState({ visible: true });

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  registerUser = () => {
    if(this.state.email === '' || this.state.password === '' || this.state.nik === '' || this.state.name === '') {
      this._showDialog()
      this.setState({
        error: "Dimohon Untuk Melengkapi Form"
      })
    } else {
      this.setState({
        isLoading: true,
        disabledInput: true,
      })
      firebase
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then((res) => {
        this.setState({
          id_user: res.user.uid
        })
        this.detailData()
      })
      .catch(error => this.setState({ errorMessage: error.message }))
    }
  }

  userLogin = () => {
    firebase
    .auth()
    .signInWithEmailAndPassword(this.state.email, this.state.password)
    .then((res) => {
      this.props.navigation.navigate('Draw')
    })
    .catch((error) => {
      this.props.navigation.navigate('LoginScreen')
    })
  }

  detailData() {
    if(this.state.id_user) {
      const dbRef = firebase.firestore().collection('users').doc(this.state.id_user)
        dbRef.set({
          name: this.state.name,
          nik: this.state.nik,
          role: 'Admin'
      })
    }
    this.userLogin()
  }

  updateInputNik = (nik) => {
    let newText = '';
    let numbers = '0123456789';

    for (var i=0; i < nik.length; i++) {
        if(numbers.indexOf(nik[i]) > -1 ) {
            newText = newText + nik[i];
        }
        else {
            // your call back function
        this._showDialog()
        this.setState({
          error: 'Masukkan Hanya Angka Saja!'
        })
        }
    }
    this.setState({ nik: newText });
  }

  render() {
    return (
      <PaperProvider>
        <Portal>
          <Dialog
            visible={this.state.visible}
            onDismiss={this._hideDialog}>
            <Dialog.Title>Error!</Dialog.Title>
            <Dialog.Content>
              <Paragraph> { this.state.error } </Paragraph>
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={() => this._hideDialog()}>Ok</Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
        <View style={styles.container}>
          <Title style={styles.title}>Sign Up</Title>
          <TextInput
            disabled={this.state.disabledInput}
            style={styles.inputStyle}
            placeholder="NIK"
            value={this.state.nik}
            maxLength={16}
            onChangeText={(nik) => this.updateInputNik(nik)}
          />
          <TextInput
            disabled={this.state.disabledInput}
            style={styles.inputStyle}
            placeholder="Nama"
            value={this.state.name}
            onChangeText={(val) => this.updateInputVal(val, 'name')}
          />
          <TextInput
            disabled={this.state.disabledInput}
            style={styles.inputStyle}
            placeholder="Email"
            value={this.state.email}
            onChangeText={(val) => this.updateInputVal(val, 'email')}
          />
          <TextInput
            disabled={this.state.disabledInput}
            style={styles.inputStyle}
            placeholder="Password"
            value={this.state.password}
            onChangeText={(val) => this.updateInputVal(val, 'password')}
            secureTextEntry={true}
          />
          { this.state.isLoading === true ?
            <View style={{ marginTop: 20 }}>
            <ActivityIndicator size="small" animating={true} color={Colors.blue400} />
            </View> :
            <View style={{ marginTop: 20 }}>
              <Button
                style={styles.button}
                onPress={() => this.registerUser()}
                mode="contained"
              >
              Sign Up
              </Button>
              <TouchableRipple
                onPress={() => this.props.navigation.navigate('LoginScreen')}
                style={{ marginTop:25, height: 25, justifyContent: 'center' }}
              >
                <Text style={styles.loginText}>
                  Already Registered? Click here to Login
                </Text>  
              </TouchableRipple>
            </View>
          }
        </View>
      </PaperProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: 35,
    backgroundColor: '#fff'
  },
  inputStyle: {
    backgroundColor: 'white',
    marginBottom: 15,
  },
  loginText: {
    color: '#4AB19D',
    textAlign: 'center'
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  title: {
    textAlign: "center",
    marginBottom: 50,
    fontSize: 24
  },
  button: {
    height: 50,
    marginTop: 20,
    borderRadius: 5,
    justifyContent:'center'
  }
});
export default SignUpScreen;