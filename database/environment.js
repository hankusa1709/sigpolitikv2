// change the file name dummy.environment.js to "environment.js"
// and add your keys below

import * as Expo from 'expo';

var environments = {
	staging: {
		FIREBASE_API_KEY: 'AIzaSyAcXfUcvmPDwVGFS5oHX7v9Oj_Z3zlWXfg',
		FIREBASE_AUTH_DOMAIN: 'sigpol-17.firebaseapp.com',
		FIREBASE_DATABASE_URL: 'https://sigpol-17.firebaseio.com',
		FIREBASE_PROJECT_ID: 'sigpol-17',
		FIREBASE_STORAGE_BUCKET: 'sigpol-17.appspot.com',
		FIREBASE_MESSAGING_SENDER_ID: '50002567882',
		GOOGLE_CLOUD_VISION_API_KEY: 'AIzaSyCydCaKqoE8wGKyqfh7KmvXhni7wLzntAc',
		AZURE_SUBS_KEY: '5f6ecd733436416ca7a74e9a11f6d3f4',
		GOOGLE_CLOUD_VISION_API_KEY: 'AIzaSyCLP1FTJSmvqUywESQvveyYdCeaHucsAL0'
	},
	production: {
		// Warning: This file still gets included in your native binary and is not a secure way to store secrets if you build for the app stores. Details: https://github.com/expo/expo/issues/83
	}
};

function getReleaseChannel() {
	return 'staging';
}
function getEnvironment(env) {
	console.log('Release Channel: ', getReleaseChannel());
	return environments[env];
}
var Environment = getEnvironment(getReleaseChannel());
export default Environment;